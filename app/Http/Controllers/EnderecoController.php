<?php

namespace App\Http\Controllers;

use App\Http\Requests\EnderecoEditRequest;
use App\Models\Endereco;
use App\Services\EnderecoService;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class EnderecoController extends Controller
{

    public EnderecoService $service;

    public function __construct(EnderecoService $service)
    {
        $this->service = $service;
    }

    /**
     * Retorna o endereco de um cidadao conforme o cpf
     * 
     * @param   string  $cpf
     * @return  \Illuminate\Http\JsonResponse;
     */
    public function show($cpf)
    {
        $address = $this->service->show($cpf);

        if (!$address instanceof Endereco) {
            return response()
                ->json([], Response::HTTP_NOT_FOUND);
        }
        return response()
            ->json($address, Response::HTTP_OK);
    }

    /**
     * Atualiza o endereco de um cidadao conforme o cpf
     * 
     * @param   string                                  $cpf
     * @param   \App\Http\Requests\EnderecoEditRequest  $request
     * @return  \Illuminate\Http\JsonResponse;
     */
    public function update($cpf, EnderecoEditRequest $request): JsonResponse
    {
        $address = $this->service->show($cpf);

        if (!$address instanceof Endereco) {
            return response()
                ->json($address, Response::HTTP_NOT_FOUND);
        }

        $updated = $this->service->update(
            $address,
            $request->validated()
        );

        if (!$updated instanceof Endereco) {
            return response()
                ->json($updated, Response::HTTP_CONFLICT);
        }
        return response()->json($updated, Response::HTTP_OK);
    }


}
