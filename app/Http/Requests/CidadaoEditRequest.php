<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CidadaoEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'nullable|min:2',
            'sobrenome' => 'nullable|min:2',
            'cpf' => 'nullable|unique:App\Models\Cidadao,cpf',
        ];
    }



    /**
     * Get the messages
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'nome.min' => 'O nome deve ser preenchido com um valor valido (Min 2 caracter)',
            'sobrenome.min' => 'O sobrenome deve ser preenchido com um valor valido (Min 2 caracter)',
            'cpf.unique' => 'CPF já cadatrado. Por favor, confira os dados ou entre em contato conosco',
        ];
    }
}
