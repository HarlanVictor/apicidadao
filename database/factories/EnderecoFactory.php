<?php

namespace Database\Factories;

use App\Models\Endereco;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Services\EnderecoService;

class EnderecoFactory extends Factory
{

    private array $cepList = [
        '86046200',
        '94960838',
        '36770070',
        '78158620',
        '36302616',
        '79103650',
        '13173210',
        '93260530',
        '69909654',
        '69020760'
    ];
    
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Endereco::class;

    
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return $this->configNewAdress($this->cepList[random_int(0,9)]);

    }

    private function configNewAdress($cep)
    {
        
        $address = EnderecoService::importAddres($cep);
        return [
            "cep" => $cep,
            "logradouro" => $address["logradouro"],
            "bairro" => $address["bairro"],
            "cidade" => $address["localidade"],
            "estado" => $address["uf"],
        ];

    }
}
